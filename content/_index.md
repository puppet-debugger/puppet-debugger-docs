+++
title = "Puppet Debugger Documentation"
date = 2019-05-21T14:22:53-07:00
weight = 1

+++

# **Puppet Debugger Documention**
![debugger](images/debugger.png)

Learn how to use the debugger by getting started [here](getting_started/)