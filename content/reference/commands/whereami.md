---
title: "Whereami"
date: 2019-07-03T19:17:13-04:00
draft: true
---

The whereami command is solely used in combination with the `debug::break` [function](https://github.com/nwops/puppet-debug).

The purpose is to show you where in the code the break function is pointing at.  You can call this command at any time.