---
title: "Known resource types"
date: 2019-07-03T19:18:39-04:00
draft: false
---

List all of the known resource types with the command `krt`.  This shows all the resource types
in the current catalog.

```
5:>> krt
{
 :applications        => [],
 :capability_mappings => [],
 :definitions         => [],
 :hostclasses         => [
  [0] "",
  [1] "settings",
  [2] "stdlib",
  [3] "stdlib::stages"
 ],
 :nodes               => [],
 :sites               => nil
}
```
