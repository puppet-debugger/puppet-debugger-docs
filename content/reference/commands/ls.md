---
title: "Ls"
date: 2019-07-03T19:18:58-04:00
draft: false
---

When writing puppet code there will be times when you need to see the value of a variable. Inside the debugger you 
can just type the variable name. The `ls` command will list all the variables currently set, in the current scope and even an the scope of a evaluated resource. 

The scope used during Puppet compilation can be inspected at any time with the `ls` comamnd.

Use the `vars` or `ls` command to show the contents of the current scope.

```
8:>> ls
"Facts were removed for easier viewing"
{
 "datacenter"   => "datacenter1",
 "facts"        => "removed by the puppet-debugger",
 "module_name"  => "",
 "name"         => "main",
 "server_facts" => {
  "environment"   => "production",
  "serverip"      => "10.0.3.154",
  "servername"    => "MacBook-Pro-162",
  "serverversion" => "6.4.0"
 },
 "title"        => "main",
 "trusted"      => {
  "authenticated" => "local",
  "certname"      => nil,
  "domain"        => nil,
  "extensions"    => {},
  "hostname"      => nil
 }
}
```

### Parameters inside a resource

Use `ls` to see the evaulated parameters.  Even works when hiera lookups are performed.

Here I defined the foo class with two parameters.  Then I used the foo class and passed in 
a value for $param1 parameter.  Running `ls` then showed the value of $param2 as it was evaluated.

```
5:>> class foo( $param1, $param2 = "${param1}/bar"){
  6:>> }
7:>> class{'foo': param1 => '/tmp/test' }
 => Puppet::Type::Component {
  loglevel => notice,
      name => "Foo",
     title => "Class[Foo]"
}
8:>> ls foo
{
 "Class[Foo]" => {
  "param1" => "/tmp/test",
  "param2" => "/tmp/test/bar"
 }
}
```