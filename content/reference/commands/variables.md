+++
title = "Variables"
date = 2019-05-21T16:12:55-07:00
weight = 6
chapter = false
#pre = "<b>X. </b>"
+++

Variables in puppet code help us remove statically defined data. Unfornately they also have the side affect of being misinterperted due to logic bugs or unforseen circumstances.

You can use the puppet debugger to see the values of all variables no matter which scope they are defined in. This allows you to see problems ahead of time or even help validate your logic.

```
8:>> $file_base_path = "/tmp"
 => "/tmp"
9:>> $file_path = "${file_base_path}/test.txt"
 => "/tmp/test.txt"
10:>>
```

Listing all the variables

```
10:>> ls
"Facts were removed for easier viewing"
{
 "datacenter"     => "datacenter1",
 "facts"          => "removed by the puppet-debugger",
 "file_base_path" => "/tmp",
 "file_path"      => "/tmp/test.txt",
 "module_name"    => "",
 "name"           => "main",
 "server_facts"   => {
  "environment"   => "production",
  "serverip"      => nil,
  "servername"    => "MacBook-Pro-162.local",
  "serverversion" => "6.4.0"
 },
 "title"          => "main",
 "trusted"        => {
  "authenticated" => "local",
  "certname"      => nil,
  "domain"        => nil,
  "extensions"    => {},
  "hostname"      => nil
 }
}

```

Show any variable simply by typing the name of the variable just like you would in a manifest.

```
7:>> $os
 => {
  "architecture" => "x86_64",
        "family" => "RedHat",
      "hardware" => "x86_64",
          "name" => "Fedora",
       "release" => {
     "full" => "23",
    "major" => "23"
  },
       "selinux" => {
       "config_mode" => "permissive",
     "config_policy" => "targeted",
      "current_mode" => "permissive",
           "enabled" => true,
          "enforced" => false,
    "policy_version" => "29"
  }
}
```

**TIP** Use tab to auto complete the variable name. 