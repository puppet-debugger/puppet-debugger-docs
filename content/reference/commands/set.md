---
title: "Set"
date: 2019-07-03T19:18:51-04:00
draft: false
---

The set command is used to set various puppet debugger settings.  The loglevel is probably the most useful.

```
11:>> set loglevel debug
loglevel debug is set

```

Currently the only settings are loglevel and node.  You can use autocomplete (tab) to help you 
find the options for the setting.

