---
title: "Functions"
date: 2019-07-03T19:17:47-04:00
draft: false
---

Puppet Debugger was designed to help you determine what a function returns in real time as well as what functions are available from each module.

The debugger will search your module path for functions and present you with a nice list of functions for you to explorer and execute.

The debugger outputs a table that displays the name and the module from which it was found. Functions found in Puppet core are labeled with the version of Puppet currently running.

To list the functions just use the command `functions`

```
4:>> functions
FULL_NAME                  | MOD_NAME
---------------------------|-------------
abs                        | puppet-6.4.0
alert                      | puppet-6.4.0
all                        | puppet-6.4.0
annotate                   | puppet-6.4.0
any                        | puppet-6.4.0
any2array                  | stdlib
any2bool                   | stdlib
assert_private             | stdlib
assert_type                | puppet-6.4.0
base64                     | stdlib
basename                   | stdlib
binary_file                | puppet-6.4.0
bool2num                   | stdlib
bool2str                   | stdlib

```
