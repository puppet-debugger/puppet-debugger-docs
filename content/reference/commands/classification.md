---
title: "Classification"
date: 2019-07-03T19:17:19-04:00
draft: false
---

## Remote nodes and classification
The puppet-debugger  has the ability to pull in remote node information. Instead of mocking facts you can pull in real information!  Get real facts and real node classification from the puppet master and then play with the info in the debugger session. This is made possible by using the puppet node indirector interface.  What this ultimately means is debugging a node in production without needing access to the node.  

ie.  `puppet node find hostname --terminus rest --render-as yaml`

In order to use this feature you will need to setup a few things.

### Setup
1. Allow node rest calls from your workstation on the puppet master(s)
See https://github.com/nwops/puppet-debugger/issues/17 for more info on allowing this rule.  

2. Ensure you have the same code that your puppet master does.  Since this pulls in classification you will need to have all the puppet modules defined in your environmentpath or basemodulepath.   The simple thing to do is to point your basemodulepath to your fixtures directory if using rspec-puppet or some other high level modules directory.   For PE users you will want to ensure you have all the
pe specific modules in your development environment.  Not doing this step will result in class not found errors.

3. Set the server config in your local puppet.conf. (on your development machine)

    a. Find your puppet config file `puppet config print config`

    b. edit the config file with an editor

    c. add `server = <hostname>` to your main block

    d. you can also start the debugger with the `puppet debugger --server <hostname>` config set.  

4. If using hiera you also need to set the path to your hiera config file since
   the puppet-debugger will be compiling puppet code which could in turn call hiera
   functions.  Additionally, make sure your hiera config points to the correct
   data location.

  ```
  [main]
    server = pe-puppet.localdomain
    basemodulepath = /Users/cosman/github/puppet-debugger-demo/modules:/Users/cosman/Downloads/pe_modules
    hiera_config = /Users/cosman/github/puppet-debugger-demo/hieradata/hiera.yaml


  ```
{{% notice note %}}
If using hiera-eyaml or similar plugin that requires keys normally stored on the master you would also need those keys present on your system.
{{% /notice %}}

### Usage
There are two ways of using the remote node feature.  You can either
pass in the node name from the command line or set the node name from the debugger session.

#### Command line:
`puppet debugger -n node_name`

![command line](../../../images/command_line_node.png)

#### From debugger session:
`set node node_name`

![command line](../../../images/set_node.png)


This is also extremely useful to check classification rules and variables by your ENC. So instead of running your ENC script manually we get the puppet master to run this indirectly and return the results to you.
