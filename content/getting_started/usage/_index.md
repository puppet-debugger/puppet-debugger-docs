+++
title = "Basic Usage"
date = 2019-05-21T14:22:53-07:00
weight = 3
#chapter = false
#pre = "<b>X. </b>"
+++

Once you have [installed](../installation) the debugger you just need to execute puppet with the debugger subcommand.

```shell
$ puppet debugger
Ruby Version: 2.5.1
Puppet Version: 6.4.0
Puppet Debugger Version: 0.12.0
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.


1:>>
```

Once you have the debugger started, use the `commands` command to show a list of [commands](/reference/commands).

At any point you can type any valid puppet code.  Start with a few simple things then move onto more advanced puppet concepts.  Just remember "garbage in, garbage out".  Syntax errors, duplicate resources, and variable re-definations will show their relevant errors.  

1. Variable assignment: `$var1 = 'kaboom!'`
2. Run a function: `sha1('12345')`
3. include a class `include stdlib`


### PDK Usage
With the future inclusion of the `pdk console` command in the PDK.  The puppet debugger has never been easier to use.  Additionally the PDK also exposes some unique features only included with the PDK.

The `pdk console` command executes a session of the puppet debugger when inside a module and allows for exploration of puppet code.  

To use, execute `pdk console` from inside your module directory.  You can also supply the `--puppet-version` or `--pe-version` or `--puppet-dev` to swap out the puppet version when using the console.  This is the unique feature that PDK provides for the debugger and is extremely valuable when determine differences between puppet versions.

Example (from within a module):

* `pdk console --puppet-version=5`
* `pdk console --pe-version=2018.1`

PLease note when using the new `pdk console` command the same puppet debugger options and arguments will still apply as they are simply passed through to the puppet debugger.  

Example:

`puppet debugger --no-facterdb --run-once -e "$os"` 

vs 

`pdk console --no-facterdb --run-once -e "$os"`


