+++
title = "Custom Ruby"
date = 2019-05-21T14:22:53-07:00
weight = 7
#chapter = false
#pre = "<b>X. </b>"
+++

If you have your own custom ruby installed or you are using the one provided with the OS, just use the gem
command to install the debugger.

`gem install puppet-debugger`.

#### Installing in user-supplied Ruby

First you will need Ruby >= 2.4 or whatever the Puppet version you have installed supports.

Then it is just a matter of installing the gem (user space).

`gem install puppet-debugger --no-ri --no-rdoc --user-install`

If you see this warning, make sure you add the specified path to your path variable.

```
WARNING:  You don't have /home/user_1/.gem/ruby/2.4.0/bin in your PATH,
	  gem executables will not run
export PATH=$PATH:/home/user_1/.gem/ruby/2.4.0/bin
```

