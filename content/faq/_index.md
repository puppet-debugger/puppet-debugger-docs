+++
title = "Frequently Asked Questions"
date = 2019-05-21T14:22:53-07:00
weight = 6
#chapter = false
#pre = "<b>X. </b>"
+++

Before we start, please read the following questions and answers as a primer to discover why I built this tool and how it can help solve puppet development problems.

### What is a Debugger?

A debugger is a tool present in almost all programming languages that allows you to see how the compiler evaluates your code as if you were standing inside the compiler with a poking stick.

Many debuggers and REPLs can be found [online](https://repl.it/languages)

Examples of other existing debuggers or REPLs.

- irb
- python
- go playground
- js console

### Why do we need a debugger for Puppet code?

Puppet code can become quite complex when using some of the advanced functionality found inside the Puppet language. Additionally, Puppet code is used across multiple devops tools like Bolt so it makes sense to have a way to debug Puppet code when a unit test is not possible or feasible.

Below are just a few reasons why you would use a debugger:

- Use with Bolt for plans and tasks
- Use with datatypes
- Use with Hiera
- Use with native functions
- Use in defined types and classes

### Is Puppet Debugger a unit test replacement?

Puppet Debugger **is not** a replacement for proper unit testing. Unit testing is used to catch errors across code changes while a debugger does not. However, Puppet Debugger can be used to to evaluate complex code where you might have used a unit test before to dump output to STDOUT.

### What does Puppet Debugger replace?

Using Puppet's traditional debugging methods is inefficient when compared to a live interactive console. Puppet Debugger replaces these archaic methods:

- slow puppet apply and notify resource output
- dump_args, and echo functions
- other crafty ways to get variable values into STDOUT

Puppet Debugger will show you the before, during, and after state, while other tools show the before and after state only.

### What makes Puppet Debugger unique?

- Pluggable expansion system, all debugger commands are plugins
- Works with multiple Puppet versions
- Community driven
- Emulates a real or fake node

### Where you can run Puppet Debugger?

Puppet Debugger was designed to run on all major platforms. Only requirement is a modern ruby installation.

- Linux
- Mac
- Windows

### Which Puppet versions does the debugger support?

- Puppet 5 (test with 5.5.x)
- Puppet 6 (tested with 6.X)
- Puppet 7 (tested with 7.X)


### What kinds of things can a normal debugger do?

- Inspect variables (see values)
- Run code
- Set breakpoints
- Evaluate code
- Skip, break, retry, jump in, jump out of code blocks

### What can Puppet Debugger do?

- Inspect variables in various scopes
- View facts and their structures
- List and run functions in scope
- Playback code
- Run arbitrary code
- Create classes, functions, defined types, datatypes, plans, tasks
- Lookup data with Hiera lookup calls
- Mock other operating systems
- Set breakpoints

### What does Puppet Debugger not do?

- Cannot be used to enforce the catalog
- Should never run with Puppet agent catalog compilation
- skip, retry, jump in, jump out (currently not implemented)

### Where can Puppet Debugger be used?

ANY PUPPET CODE. ANY.

- In combination with unit tests using the debug::break() function
- Used instead of puppet apply or even with puppet apply using the debug::break() function
- Puppet tasks and plans
- Puppet native functions
- Puppet classes and defined types
- Puppet data types

### Why is Puppet Debugger better than the normal puppet apply?

Puppet apply has been the go-to method for evaluating code. However, it is slow and requires the usage of notify resources or other functions to dump values to STDOUT. Puppet Debugger offers:

- Faster results
- Interactive
- No need to write notify resources
- Can even use one-liners for instant gratification

### Why do I get could not find class error?

```
=> Evaluation Error: Error while evaluating a Function Call, 
Could not find class ::<insert name of class> for foo.example.com 
(file: /tmp/puppet_debugger_input20200421-14380-1b48jnn.pp, line: 1, column: 1)
```

The following error is a result of the class not being in your [modulepath](https://puppet.com/docs/puppet/5.5/dirs_modulepath.html).
When puppet compiles the catalog it searches the modulepath you provided it to find
the classes.  You can provide the modulepath in many ways but the simplest
is via the `--basemodulepath` option.

`puppet debugger --basemodulepath=/path/to/modules`  # This must be a directory of modules.

`puppet debugger --basemodulepath=spec/fixtures/modules` # use when inside a module with fixtures

