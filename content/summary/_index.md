+++
title = "Introduction"
date = 2019-05-21T15:50:22-07:00
weight = 1
chapter = false
#pre = "<b>1. </b>"
+++

The debugger is a puppet application packaged as a ruby gem installed along side puppet.

The primary purpose is to give you an interactive console to debug puppet code and allow you tinker with the puppet language.

Since the beginning of puppet there has never been a simple way to run arbitrary puppet code without affecting a real system. We have always used simple variable dump hacks like puppet apply and a notify resource to spit out the result of some code. The debugger replaces that entire `puppet apply` hack with a simple interactive console.

Almost all programming languages provide a debugging console that let you play around with code interactively, except Puppet.  Thus the debugger was born.

The debugger parses and evaluates your code and shows you the resultant value instantly.  Immediate feedback helps you learn faster, gain insight into how the language or your code will actually work. 

Start understanding and debugging your code with the [getting started](../getting_started/) guide.

![](../images/animated-debugger-demo.gif)
